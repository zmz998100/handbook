---
bookFlatSection: true
---

# Shortcode Samples

- [buttons]({{< relref "buttons" >}})
- [columns]({{< relref "columns" >}})
- [expand]({{< relref "expand" >}})
- [hints]({{< relref "hints" >}})
- [katex]({{< relref "katex" >}})
- [mermaid]({{< relref "mermaid" >}})
- [tags]({{< relref "tags" >}})